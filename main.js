const express = require('express');
const app = express()
const port = 3000


/**  
 * Function that greets people based on their name
 * @param {string} name 
 * @returns {string} 
 *  
 * */

const greet = (name) =>{
    if(!!name) return "Hello "+ name; 
    return "Provide a name please"

}


app.get('/', (req, res) => {
  res.send(greet('Sir'));
})


app.use((req, res, next) => {
    res.status(404).send("Sorry can't find that!")
  })

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
